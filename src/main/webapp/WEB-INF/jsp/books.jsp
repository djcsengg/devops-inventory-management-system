<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
    <%@ page isELIgnored="false" %>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Product Library</title>
    
    
    <style>
  body {
  background-color: lightblue;
}
    button{
    background-color:'turquoise';
    height:50x;
    width:200px;
    font-size: 14px;
    }
    
    
    </style>
</head>
<body>
    <div class="bookhome">
        <div align="center">
            <h1>Product Inventory Management</h1>
            
            
           
            <br/>
            <div>
                <div>
                    <h2>Product List</h2>
                </div>
                <div>
                    <table border="1" cellpadding="10" >
                        <thead>
                         <tbody>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Price</th>
                            <th>Category</th>
                            <th>Action</th>
                        </tr>
                                </thead>
                        <c:forEach var="book" items="${books}">
                            <tr>
                                <td>${book.id}</td>
                                   <td>${book.name}</td>
                                <td>${book.price}</td>
                                <td>${book.category}</td>
                             
                                <td>
                                    <a href="/book/${book.id}">Edit</a>
                                    &nbsp;&nbsp;&nbsp;
                                    <form action="/book/${book.id}/delete" method="post">
                                        <input type="submit" value="Delete" />
                                    </form>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                     <br/> <br/> <br/>
                     <a href="/new-book">
                <button class="addproduct" type="submit">Add New Product</button>
                
            </a>
                </div>
            </div>
        </div>
    </div>
</body>
</html>