<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html lang="en">
<head>
    <%@ page isELIgnored="false" %>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Product Inventory</title>
    
    <style>
    
     body {
  background-color: lightblue;
}
    .edit-form{
        width: 396px;
    margin: 150px auto;
    }
    .edit-form form {        
    margin-bottom: 15px;
    background:turquoise;
   
    padding: 30px;
}

.form-control {
    min-height: 25px;
    border-radius: 2px;}
    
    .form-group{
    padding-top:10px;
    padding-bottom:10px;
}
    
    </style>
    
    
</head>
<body>
    <div align="center">
        
        <div>
            <div class="edit-form">
            <h2>Edit Product</h2>
                <form:form action="/book/${book.id}/update" modelAttribute="book" method="post">
                    <div>
                        <div >
                           <h3>Id: ${book.id} </h3>
                        </div>
                        <div class="form-group">
                            <form:label path="price">Price</form:label>
                            <form:input type="text" class="form-control" id="price" path="price"/>
                            <form:errors path="price" />
                        </div>
                        <div class="form-group">
                            <form:label path="name">Name</form:label>
                            <form:input type="text" class="form-control" id="name" path="name"/>
                            <form:errors path="name" />
                        </div>
                         <div class="form-group">
                            <form:label path="category">Category</form:label>
                            <form:input type="text" class="form-control" id="category" path="category"/>
                            <form:errors path="category" />
                        </div>
                    </div>
                    <div>
                        <div>
                            <input type="submit" value="Update Product">
                        </div>
                    </div>
                </form:form>
            </div>
        </div>
    </div>
    </body>
</html>