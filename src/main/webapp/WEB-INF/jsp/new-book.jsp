<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html lang="en">
<head>
    <%@ page isELIgnored="false" %>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Product Inventory</title>
    
     <style>
     
      body {
  background-color: lightblue;
}
    .add-form{
        width: 396px;
    margin: 150px auto;
    }
    .add-form form {        
    margin-bottom: 15px;
    background:turquoise;
   
    padding: 30px;
}

.errors{
txt-color:red;
}
.form-control {
    min-height: 25px;
    border-radius: 2px;}
    
    .form-group{
    padding-top:10px;
    padding-bottom:10px;
}
    
    </style>
</head>
<body>
    <div align="center">
       
        <div>
            <div class="add-form">
             <h2>New Product</h2>
                <form:form action="/add" modelAttribute="book" method="post">
                    <div class="form-group">
                        
                        <div>
                            <form:label path="name">Name</form:label>
                            <form:input type="text" class="form-control" id="name" path="name"/>
                              <br/>
                            <form:errors class="errors" path="name" />
                        </div> 
                        <div class="form-group">
                            <form:label path="price">Price</form:label>
                            <form:input type="text" class="form-control" id="price" path="price"/>
                            <br/>
                            <form:errors class="errors" path="price" />
                        </div>
                        <div class="form-group">
                            <form:label path="category">Category</form:label>
                            <form:input type="text" class="form-control" id="category" path="category"/>
                              <br/>
                            <form:errors class="errors" path="category" />
                        </div>
                        
                    </div>
                    <div>
                        <div>
                            <input background-color:'turquoise' type="submit" value="Add Product">
                        </div>
                    </div>
                </form:form>
            </div>
        </div>
    </div>
    </body>
</html>